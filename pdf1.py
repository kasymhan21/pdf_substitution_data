# -*- coding: utf-8 -*-

import re
import json
import os

from sys import argv
from docx import Document


def main(template_file, data):
	#функция для замены выражения в docx
    def docx_replace_regex(doc_obj, regex , replace):
        for p in doc_obj.paragraphs:
            if regex.search(p.text):
                inline = p.runs
                for i in range(len(inline)):
                    if regex.search(inline[i].text):
                        text = regex.sub(replace, inline[i].text)
                        inline[i].text = text

        for table in doc_obj.tables:
            for row in table.rows:
                for cell in row.cells:
                    docx_replace_regex(cell, regex , replace)


    filename = open(template_file)

    with open(data) as json_data:
        information = json.load(json_data)# для парсинга json file

    count=0

    for word in information:
        print(word)

        doc = Document(template_file)
        
        for key in word:
            print(key)
            regex1 = re.compile("{{"+key+"}}")
            docx_replace_regex(doc, regex1 , word[key])

        output_file = 'result'+str(count)+'.docx'
        print("Save in " + output_file)
        
        doc.save(output_file)
        
        os.system("doc2pdf " + output_file)
        
        count+=1  


if __name__ == '__main__':
    if len(argv) == 3:
        main(argv[1],argv[2])
    else:
        print("Input 2 arguments: template_file(docx), data(json) ")
